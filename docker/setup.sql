
CREATE TABLE disciplines (
  id bigserial NOT NULL PRIMARY KEY,
  name varchar(65) NOT NULL
);

CREATE TABLE speciality (
  id bigserial NOT NULL PRIMARY KEY,
  name varchar(30) NOT NULL
);

CREATE TABLE students (
  id bigserial NOT NULL PRIMARY KEY,
  speciality_id bigint NOT NULL REFERENCES speciality(id),
  last_name varchar(30) NOT NULL,
  name varchar(30) NOT NULL,
  surname varchar(30) NOT NULL,
  date_of_study date,
  form_of_study smallint CHECK (form_of_study BETWEEN 0 AND 2)
);

CREATE TABLE specialty_discipline (
  discipline_id bigint NOT NULL,
  specialty_id bigint NOT NULL,
  PRIMARY KEY (discipline_id, specialty_id),
  FOREIGN KEY (discipline_id) REFERENCES disciplines(id),
  FOREIGN KEY (specialty_id) REFERENCES speciality(id)
);

CREATE TABLE disc_in_sem (
  id bigserial NOT NULL PRIMARY KEY,
  semester integer NOT NULL,
  count_hour integer,
  reporting_form smallint CHECK (reporting_form BETWEEN 0 AND 1),
  discipline_id bigint NOT NULL REFERENCES disciplines(id)
);

CREATE TABLE journal (
  id bigserial NOT NULL,
  student_id bigint NOT NULL REFERENCES students(id),
  discipline_id bigint NOT NULL REFERENCES disc_in_sem(id),
  date date NOT NULL,
  grade smallint CHECK (grade BETWEEN 0 AND 3),
  semester integer NOT NULL,
  PRIMARY KEY (date, id)
)
PARTITION BY RANGE (date);

