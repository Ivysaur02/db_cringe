package com.example.demo.domain;

import com.example.demo.model.enums.ReportingForm;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DisciplineCountHoursAndForm {

    private String disciplineName;

    private Long countHours;

    private ReportingForm reportingForm;

}