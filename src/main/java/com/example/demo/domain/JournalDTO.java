package com.example.demo.domain;

import com.example.demo.model.enums.Grade;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JournalDTO {

    private Long id;
    private String name;
    private String lastname;
    private String surname;
    private String disciplineName;
    private String grade;
}
