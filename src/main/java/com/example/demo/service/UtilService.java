package com.example.demo.service;


import com.example.demo.model.*;
import com.example.demo.model.enums.FormOfStudy;
import com.example.demo.model.enums.Grade;
import com.example.demo.model.enums.ReportingForm;
import com.example.demo.repo.*;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

@Service
public class UtilService {

    private final DisInSemRepository disInSemRepository;
    private final DisciplineRepository disciplineRepository;
    private final JournalRepository journalRepository;
    private final SpecialtyRepository specialtyRepository;
    private final StudentRepository studentRepository;

    public UtilService(DisInSemRepository disInSemRepository, DisciplineRepository disciplineRepository,
                       JournalRepository journalRepository, SpecialtyRepository specialtyRepository,
                       StudentRepository studentRepository) {
        this.disInSemRepository = disInSemRepository;
        this.disciplineRepository = disciplineRepository;
        this.journalRepository = journalRepository;
        this.specialtyRepository = specialtyRepository;
        this.studentRepository = studentRepository;
    }

    public void startFillValues() {
        Random r = new Random();

        ArrayList<Discipline> disciplines = new ArrayList<>();
        ArrayList<Speciality> specialities = new ArrayList<>();
        ArrayList<Student> students = new ArrayList<>();
        List<DisciplineInSemester> disciplinesInSemester = new ArrayList<>();
        List<PerformanceJournal> performanceJournals = new ArrayList<>();

        List<String> specialityNames = List.of("МОАИС", "ИБ", "ФИИТ", "ПМИ", "ФММ");

        List<String> studentsNames = List.of("Иванов", "Иван", "Иванович",
                "Петрова", "Мария", "Сергеевна",
                "Сидоров", "Николай", "Петрович",
                "Васильева", "Анна", "Владимировна",
                "Кузнецов", "Михаил", "Игоревич",
                "Соколова", "Ольга", "Александровна",
                "Попов", "Андрей", "Дмитриевич",
                "Захарова", "Екатерина", "Алексеевна",
                "Романов", "Сергей", "Викторович"
        );

        disciplines.add(Discipline.builder().name("Математический анализ").build());
        disciplines.add(Discipline.builder().name("Дифференциальные уравнения").build());
        disciplines.add(Discipline.builder().name("Дискретная математика").build());
        disciplines.add(Discipline.builder().name("Алгебра и теория чисел").build());
        disciplines.add(Discipline.builder().name("Теория вероятностей и математическая статистика").build());
        disciplines.add(Discipline.builder().name("Геометрия и топология").build());
        disciplines.add(Discipline.builder().name("Физическая культура и спорт").build());
        disciplines.add(Discipline.builder().name("Элективные курсы по физической культуре и спорту").build());

        disciplineRepository.saveAll(disciplines);

        for (String specialityName : specialityNames) {
            // Создание новой специальности
            Speciality newSpecialty = Speciality.builder()
                    .name(specialityName)
                    .disciplines(new HashSet<>())
                    .build();
            // Случайное количество дисциплин
            int randomDisciplinesCount = Math.abs(r.nextInt(3)) + 1; // от 1 до 3
            // Добавление рандомных дисциплин
            for (int j = 0; j < disciplines.size(); j += randomDisciplinesCount) {
                newSpecialty.getDisciplines().add(disciplines.get(j));
            }
            specialities.add(newSpecialty);
        }
        specialtyRepository.saveAll(specialities);

        for (int i = 0; i < studentsNames.size(); i += 3) {
            String lastName = studentsNames.get(i);
            String firstName = studentsNames.get(i + 1);
            String middleName = studentsNames.get(i + 2);

            Student student = Student.builder()
                    .name(firstName)
                    .lastName(lastName)
                    .surname(middleName)
                    .speciality(specialities.get(r.nextInt(specialityNames.size())))
                    .formOfStudy(FormOfStudy.values()[Math.abs(r.nextInt(ReportingForm.values().length))])
                    .dateOfStudy(Date.valueOf("2020-09-01"))
                    .build();


            students.add(student);
        }
        studentRepository.saveAll(students);

        int count = Math.abs(r.nextInt(7)) + 5;

        for (int i = 0; i < count; i++) {
            DisciplineInSemester disciplineInSemester = new DisciplineInSemester();
            disciplineInSemester.setSemester(8);
            disciplineInSemester.setDiscipline(disciplines.get(Math.abs(r.nextInt(disciplines.size()))));
            disciplineInSemester.setCountHour(Math.abs(r.nextInt(91)) + 30);
            ReportingForm reportingForm = ReportingForm.values()[Math.abs(r.nextInt(ReportingForm.values().length))];
            disciplineInSemester.setReportingForm(reportingForm);
            disciplinesInSemester.add(disciplineInSemester);
        }

        disInSemRepository.saveAll(disciplinesInSemester);

        count = Math.abs(r.nextInt(6)) + 10;
        for (int i = 0; i < count; i++) {
            PerformanceJournal performanceJournal = new PerformanceJournal();
            String dateString = "2024-" + String.format("%02d", r.nextInt(4) + 3) + "-" + String.format("%02d", r.nextInt(28) + 1);
            performanceJournal.setDate(Date.valueOf(dateString));
            performanceJournal.setSemester(8);
            performanceJournal.setStudent(students.get(Math.abs(r.nextInt(students.size()))));
            performanceJournal.setDiscipline(disciplinesInSemester.get(Math.abs(r.nextInt(disciplinesInSemester.size()))));
            performanceJournal.setGrade(Grade.values()[Math.abs(r.nextInt(Grade.values().length))]);
            performanceJournals.add(performanceJournal);
        }

        count = Math.abs(r.nextInt(6)) + 4;
        for (int i = 0; i < count; i++) {
            PerformanceJournal performanceJournal = new PerformanceJournal();
            String dateString = "2023-" + String.format("%02d", r.nextInt(4) + 3) + "-" + String.format("%02d", r.nextInt(28) + 1);
            performanceJournal.setDate(Date.valueOf(dateString));
            performanceJournal.setSemester(8);
            performanceJournal.setStudent(students.get(Math.abs(r.nextInt(students.size()))));
            performanceJournal.setDiscipline(disciplinesInSemester.get(Math.abs(r.nextInt(disciplinesInSemester.size()))));
            performanceJournal.setGrade(Grade.values()[Math.abs(r.nextInt(Grade.values().length))]);
            performanceJournals.add(performanceJournal);
        }

        journalRepository.saveAll(performanceJournals);

    }
}
