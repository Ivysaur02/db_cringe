package com.example.demo.service;

import com.example.demo.domain.DisciplineCountHoursAndForm;
import com.example.demo.model.Discipline;
import com.example.demo.repo.DisInSemRepository;
import com.example.demo.repo.DisciplineRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DisciplineService {

    private final DisciplineRepository disciplineRepository;
    private final DisInSemRepository  disInSemRepository;

    public DisciplineService(DisciplineRepository disciplineRepository,
                             DisInSemRepository disInSemRepository) {
        this.disciplineRepository = disciplineRepository;
        this.disInSemRepository = disInSemRepository;
    }

    public List<DisciplineCountHoursAndForm> DisciplineCountHours(Long disciplineId){
        return disInSemRepository.findCountHoursAndFormByDiscipline(disciplineId);
    }

    public List<Discipline> getAllDisciplines(){
        return disciplineRepository.findAll();
    }
}
