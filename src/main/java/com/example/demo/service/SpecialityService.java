package com.example.demo.service;

import com.example.demo.model.Discipline;
import com.example.demo.model.Speciality;
import com.example.demo.repo.SpecialtyRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SpecialityService {

    private final SpecialtyRepository specialtyRepository;

    public SpecialityService(SpecialtyRepository specialtyRepository) {
        this.specialtyRepository = specialtyRepository;
    }

    public List<Speciality> getAllSpeciality() {
        return specialtyRepository.findAll();
    }
}
