package com.example.demo.service;

import com.example.demo.domain.JournalDTO;
import com.example.demo.model.PerformanceJournal;
import com.example.demo.model.enums.Grade;
import com.example.demo.repo.JournalRepository;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class JournalService {

    private final JournalRepository journalRepository;

    public JournalService(JournalRepository journalRepository) {
        this.journalRepository = journalRepository;
    }

    // CRUD операции

    public PerformanceJournal save(PerformanceJournal journalEntry) {
        return journalRepository.save(journalEntry);
    }

    public List<PerformanceJournal> findAll() {
        return journalRepository.findAll();
    }

    public PerformanceJournal findById(Long id) {
        return journalRepository.findById(id).orElse(null);
    }

    public void delete(PerformanceJournal journalEntry) {
        journalRepository.delete(journalEntry);
    }

    // Дополнительные методы

    public List<JournalDTO> getAllJournalEntriesWithStudentAndDiscipline() {
        List<Object[]> objects = journalRepository.findAllJournalEntriesData();
        return objects.stream()
                .map(this::mapToJournalDTO)
                .toList();
    }

    public List<JournalDTO> findByStudentId(Long studentId) {
        List<Object[]> objects = journalRepository.findByStudentId(studentId);
        return objects.stream()
                .map(this::mapToJournalDTO)
                .toList();
    }

   public List<JournalDTO> findByDisciplineId(Long disciplineId) {
       List<Object[]> objects = journalRepository.findByDisciplineId(disciplineId);
       return objects.stream()
               .map(this::mapToJournalDTO)
               .toList();
   }

    private JournalDTO mapToJournalDTO(Object[] object) {
        JournalDTO journalDTO = new JournalDTO();
        journalDTO.setId((Long) object[0]);
        journalDTO.setName((String) object[1]);
        journalDTO.setLastname((String) object[2]);
        journalDTO.setSurname((String) object[3]);
        journalDTO.setDisciplineName((String) object[4]);

        Short gradeValue = (Short) object[5];
        Grade grade = getGradeByValue(gradeValue);
        journalDTO.setGrade(grade != null ? grade.getDescription() : "Не сдавал");

        return journalDTO;
    }

    private Grade getGradeByValue(Short gradeValue) {
        if (gradeValue != null) {
            return switch (gradeValue) {
                case 1 -> Grade.A;
                case 2 -> Grade.B;
                case 3 -> Grade.C;
                case 4 -> Grade.D;
                default -> null;
            };
        }
        return null;
    }

}
