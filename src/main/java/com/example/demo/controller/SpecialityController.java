package com.example.demo.controller;

import com.example.demo.model.Speciality;
import com.example.demo.service.SpecialityService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/speciality")
public class SpecialityController {

    private final SpecialityService service;
    @GetMapping("/all")
    public ResponseEntity<List<Speciality>> getAllSpeciality() {
        return ResponseEntity.ok(service.getAllSpeciality());
    }
}
