package com.example.demo.controller;

import com.example.demo.domain.DisciplineCountHoursAndForm;
import com.example.demo.model.Discipline;
import com.example.demo.service.DisciplineService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/discipline")
public class DisciplineController {

    private final DisciplineService disciplineService;

    @GetMapping("/countById")
    public ResponseEntity<List<DisciplineCountHoursAndForm>> DisciplineCountHours(@RequestParam Long id){
        return ResponseEntity.ok(disciplineService.DisciplineCountHours(id));
    }

    @GetMapping("/all")
    public ResponseEntity<List<Discipline>> getAllDiscipline(){
        return ResponseEntity.ok(disciplineService.getAllDisciplines());
    }
}
