package com.example.demo.controller;

import com.example.demo.domain.JournalDTO;
import com.example.demo.model.PerformanceJournal;
import com.example.demo.service.JournalService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/journal")
public class JournalController {

    private final JournalService journalService;

    public JournalController(JournalService journalService) {
        this.journalService = journalService;
    }
    @PostMapping
    public PerformanceJournal save(@RequestBody PerformanceJournal journalEntry) {
        return journalService.save(journalEntry);
    }

    @GetMapping
    public List<PerformanceJournal> findAll() {
        return journalService.findAll();
    }

    @GetMapping("/{id}")
    public PerformanceJournal findById(@PathVariable Long id) {
        return journalService.findById(id);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        journalService.delete(journalService.findById(id));
    }


    @GetMapping("/all")
    public List<JournalDTO> getAllJournalEntriesWithStudentAndDiscipline() {
        return journalService.getAllJournalEntriesWithStudentAndDiscipline();
    }

    @GetMapping("/student/{studentId}")
    public List<JournalDTO> findByStudentId(@PathVariable Long studentId) {
        return journalService.findByStudentId(studentId);
    }

    @GetMapping("/discipline/{disciplineId}")
    public List<JournalDTO> findByDisciplineId(@PathVariable Long disciplineId) {
        return journalService.findByDisciplineId(disciplineId);
    }

}
