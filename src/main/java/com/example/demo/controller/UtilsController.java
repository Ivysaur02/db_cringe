package com.example.demo.controller;

import com.example.demo.service.UtilService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/util")
public class UtilsController {

    private final UtilService iniDataService;

    @GetMapping("/ini")
    public ResponseEntity<String> fillData() {
        iniDataService.startFillValues();
        return ResponseEntity.ok().build();
    }
}
