package com.example.demo.repo;

import com.example.demo.domain.DisciplineCountHoursAndForm;
import com.example.demo.model.DisciplineInSemester;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DisInSemRepository extends JpaRepository<DisciplineInSemester, Long> {
    @Query("SELECT new com.example.demo.domain.DisciplineCountHoursAndForm(d.discipline.name, SUM(d.countHour), d.reportingForm) "
            + "FROM DisciplineInSemester d "
            + "WHERE d.discipline.id = :disciplineId "
            + "GROUP BY d.discipline.name, d.reportingForm")
    List<DisciplineCountHoursAndForm> findCountHoursAndFormByDiscipline(@Param("disciplineId") Long disciplineId);

}
