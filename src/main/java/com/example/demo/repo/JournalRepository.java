package com.example.demo.repo;

import com.example.demo.domain.JournalDTO;
import com.example.demo.model.PerformanceJournal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JournalRepository extends JpaRepository<PerformanceJournal, Long> {

//    @Query("SELECT new com.example.demo.domain.JournalDTO(j.id, s.name, s.lastName, d.name, j.grade) "
//            + "FROM journal j "
//            + "JOIN students s ON j.student_Id = s.id "
//            + "JOIN disc_in_sem dis ON dis.id = j.discipline_Id "
//            + "JOIN disciplines d ON d.id = dis.discipline_Id "
//            + "WHERE j.discipline_Id = :disciplineId")
//    List<JournalDTO> findByDisciplineId(@Param("disciplineId") Long disciplineId);
//
//
//    @Query("SELECT new com.example.demo.domain.JournalDTO(j.id, s.name, s.lastName, d.name, j.grade) "
//            + "FROM journal j "
//            + "JOIN students s ON j.student_Id = s.id "
//            + "JOIN disc_in_sem dis ON dis.id = j.discipline_Id "
//            + "JOIN disciplines d ON d.id = dis.discipline_Id "
//            + "WHERE j.studentId = :studentId")
//    List<JournalDTO> findByStudentId(@Param("studentId") Long studentId);
//
//
//    @Query("SELECT new com.example.demo.domain.JournalDTO(j.id, s.name, s.lastName, d.name, j.grade) "
//            + "FROM journal j "
//            + "JOIN students s ON j.student_Id = s.id "
//            + "JOIN disc_in_sem dis ON dis.id = j.discipline_Id "
//            + "JOIN disciplines d ON d.id = dis.discipline_Id ")
//    List<JournalDTO> findAllJournalEntriesWithStudentAndDiscipline();


    @Query(value = "SELECT j.id, s.name, s.last_name, s.surname, d.name AS disciplineName, j.grade "
            + "FROM journal j "
            + "JOIN students s ON j.student_id = s.id "
            + "JOIN disc_in_sem dis ON dis.id = j.discipline_id "
            + "JOIN disciplines d ON d.id = dis.discipline_id", nativeQuery = true)
    List<Object[]> findAllJournalEntriesData();


    @Query(value = "SELECT j.id, s.name, s.last_name, s.surname, d.name AS disciplineName, j.grade "
            + "FROM journal j "
            + "JOIN students s ON j.student_id = s.id "
            + "JOIN disc_in_sem dis ON dis.id = j.discipline_id "
            + "JOIN disciplines d ON d.id = dis.discipline_id"
            + " WHERE j.student_id = :studentId", nativeQuery = true)
    List<Object[]> findByStudentId(@Param("studentId") Long studentId);

    @Query(value = "SELECT j.id, s.name, s.last_name, s.surname, d.name AS disciplineName, j.grade "
            + "FROM journal j "
            + "JOIN students s ON j.student_id = s.id "
            + "JOIN disc_in_sem dis ON dis.id = j.discipline_id "
            + "JOIN disciplines d ON d.id = dis.discipline_id"
            + " WHERE j.discipline_id = :disciplineId", nativeQuery = true)
    List<Object[]> findByDisciplineId(@Param("disciplineId") Long disciplineId);


}
