package com.example.demo.repo;

import com.example.demo.model.Student;
import com.example.demo.model.enums.FormOfStudy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends JpaRepository<Student,Long> {
    @Query("SELECT COUNT(s) FROM Student s WHERE s.formOfStudy = :formOfStudy")
    Long countByFormOfStudy(@Param("formOfStudy") FormOfStudy formOfStudy);
}

