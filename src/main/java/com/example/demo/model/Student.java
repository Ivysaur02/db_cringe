package com.example.demo.model;


import com.example.demo.model.enums.FormOfStudy;
import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.util.Date;


@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Table(name = "students")
public class Student implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 30)
    private String name;

    @Column(length = 30)
    private String lastName;

    @Column(length = 30)
    private String surname;

    @Column
    @Temporal(TemporalType.DATE)
    private Date dateOfStudy;

    @Enumerated(EnumType.ORDINAL)
    private FormOfStudy formOfStudy;

    @ManyToOne
    @JoinColumn(name = "specialityId")
    private Speciality speciality;
}