package com.example.demo.model;

import com.example.demo.model.enums.ReportingForm;
import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Table(name = "discInSem")
public class DisciplineInSemester implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "disciplineId")
    private Discipline discipline;

    @Column
    private Integer semester;

    @Column
    private Integer countHour;

    @Enumerated(EnumType.ORDINAL)
    private ReportingForm reportingForm;
}


