package com.example.demo.model.enums;

public enum ReportingForm {
    EXAM("Экзамен"),
    CREDIT("Зачёт");

    private final String description;

    ReportingForm(String description) {
        this.description = description;
    }

}
