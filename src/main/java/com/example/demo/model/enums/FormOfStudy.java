package com.example.demo.model.enums;

public enum FormOfStudy {

    DAYTIME("Daytime"),

    EVENING("Evening"),

    DISTANCE("Distance");

    private final String name;

    FormOfStudy(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

