package com.example.demo.model.enums;

public enum Grade {
    A("Отлично"),
    B("Хорошо"),
    C("Удовлетворительно"),
    D("Неудовлетворительно");

    private final String description;

    Grade(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
