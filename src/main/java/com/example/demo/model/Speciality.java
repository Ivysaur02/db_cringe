package com.example.demo.model;


import jakarta.persistence.*;
import jakarta.validation.Constraint;
import lombok.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Table(name = "speciality")
public class Speciality implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", length = 30)
    private String name;

    @ManyToMany
    @JoinTable(
            name = "specialty_discipline",
            joinColumns = @JoinColumn(name = "specialtyId"),
            inverseJoinColumns = @JoinColumn(name = "disciplineId")
    )
    private Set<Discipline> disciplines = new HashSet<>();
}
