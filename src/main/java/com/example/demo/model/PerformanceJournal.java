package com.example.demo.model;


import com.example.demo.model.enums.Grade;
import jakarta.persistence.*;
import lombok.*;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Table(name = "journal")
public class PerformanceJournal {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;

    @Column
    private Integer semester;

    @ManyToOne
    @JoinColumn(name = "studentId")
    private Student student;

    @ManyToOne
    @JoinColumn(name = "disciplineId")
    private DisciplineInSemester discipline;


    @Enumerated(EnumType.ORDINAL)
    private Grade grade;

}
